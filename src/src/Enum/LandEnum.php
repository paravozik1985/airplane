<?php

namespace App\Enum;

class LandEnum
{
    public const RUNWAY = 'runway';
    public const WATER = 'water';
}