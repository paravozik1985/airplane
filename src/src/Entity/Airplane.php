<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AirplaneRepository")
 */
class Airplane
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $serial_number;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Hangar", inversedBy="airplanes")
    */
    private $hangar;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AirplaneModel", inversedBy="airplanes")
     */
    private $model;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSerialNumber(): ?string
    {
        return $this->serial_number;
    }

    /**
     * @param string $serial_number
     * @return $this
     */
    public function setSerialNumber(string $serial_number): self
    {
        $this->serial_number = $serial_number;

        return $this;
    }

    /**
     * @return Hangar|null
     */
    public function getHangar(): ?Hangar
    {
        return $this->hangar;
    }

    /**
     * @param Hangar|null $hangar
     * @return $this
     */
    public function setHangar(?Hangar $hangar): self
    {
        $this->hangar = $hangar;

        return $this;
    }

    /**
     * @return AirplaneModel|null
     */
    public function getModel(): ?AirplaneModel
    {
        return $this->model;
    }

    /**
     * @param AirplaneModel $model
     * @return $this
     */
    public function setModel(AirplaneModel $model): self
    {
        $this->model = $model;

        return $this;
    }
}
