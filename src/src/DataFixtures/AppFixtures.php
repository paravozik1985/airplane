<?php

namespace App\DataFixtures;

use App\Entity\Airplane;
use App\Entity\AirplaneModel;
use App\Entity\Hangar;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $hangar = new Hangar();
        $hangar->setName('Aeroprakt');
        $manager->persist($hangar);

        $aeroprakt = new AirplaneModel();
        $aeroprakt->setName('Aeroprakt A-24'); //5
        $manager->persist($aeroprakt);

        $curtiss = new AirplaneModel();
        $curtiss->setName('Curtiss NC-4'); //3
        $manager->persist($curtiss);

        $boeing = new AirplaneModel();
        $boeing->setName('Boeing 747'); //2
        $manager->persist($boeing);

        for ($i = 0; $i < 10; $i++) {
            $airplane = new Airplane();
            if ($i < 5) {
                $model = $aeroprakt;
            } elseif ($i < 8) {
                $model = $curtiss;
            } else {
                $model = $boeing;
            }
            $airplane->setModel($model);
            $airplane->setHangar($hangar);
            $airplane->setSerialNumber(rand(1, 1000));
            $manager->persist($airplane);
        }

        $manager->flush();
    }
}
