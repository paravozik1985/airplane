<?php

namespace App\Repository;

use App\Entity\AirplaneModel;
use App\Entity\Hangar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AirplaneModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method AirplaneModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method AirplaneModel[]    findAll()
 * @method AirplaneModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AirplaneModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AirplaneModel::class);
    }

    /**
     * @param Hangar $hangar
     * @return mixed
     */
    public function getAirplaneModelsByHangar(Hangar $hangar)
    {
        return $this->createQueryBuilder('models')
            ->join('models.airplanes', 'a')
            ->where('a.hangar = :hangar')
            ->setParameter('hangar', $hangar)
            ->getQuery()
            ->execute();
    }
}
