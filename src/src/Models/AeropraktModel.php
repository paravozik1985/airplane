<?php

namespace App\Models;

use App\Enum\FlyEnum;
use App\Enum\LandEnum;
use App\Enum\TakeOffEnum;
use App\Models\Abstracts\AbstractAirplaneModel;

class AeropraktModel extends AbstractAirplaneModel
{

    /**
     * @inheritDoc
     */
    public function getFlyParams(): array
    {
        return [FlyEnum::DAYTIME, FlyEnum::GOOD_WEATHER];
    }

    /**
     * @inheritDoc
     */
    public function getLandParams(): array
    {
        return [LandEnum::RUNWAY, LandEnum::WATER];
    }

    /**
     * @inheritDoc
     */
    public function getTakeOffParams(): array
    {
        return [TakeOffEnum::RUNWAY, TakeOffEnum::WATER];
    }

    /**
     * @inheritDoc
     */
    public function canFly(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function canLand(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function canTakeOff(): bool
    {
        return true;
    }
}