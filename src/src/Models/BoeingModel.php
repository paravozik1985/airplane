<?php

namespace App\Models;

use App\Enum\FlyEnum;
use App\Enum\LandEnum;
use App\Enum\TakeOffEnum;
use App\Models\Abstracts\AbstractAirplaneModel;

class BoeingModel extends AbstractAirplaneModel
{
    /**
     * @inheritDoc
     */
    public function getFlyParams(): array
    {
        return [FlyEnum::ANY_TIME, FlyEnum::ANY_WEATHER];
    }

    /**
     * @inheritDoc
     */
    public function getLandParams(): array
    {
        return [LandEnum::RUNWAY];
    }

    /**
     * @inheritDoc
     */
    public function getTakeOffParams(): array
    {
        return [TakeOffEnum::RUNWAY];
    }

    /**
     * @inheritDoc
     */
    public function canFly(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function canLand(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function canTakeOff(): bool
    {
        return true;
    }
}