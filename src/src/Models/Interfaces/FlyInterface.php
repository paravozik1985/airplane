<?php

namespace App\Models\Interfaces;

interface FlyInterface
{
    public function canFly(): bool;
}