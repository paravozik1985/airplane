#!/usr/bin/env bash

docker-compose build #--no-cache
docker-compose up -d

docker-compose exec php-fpm composer install
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate -no-interaction