#!/usr/bin/env bash

docker-compose exec php-fpm php bin/console doctrine:fixtures:load --no-interaction
docker-compose exec php-fpm php bin/phpunit